package org.mpiman.camus.app.common;

public class CommonUtils {
	
	/** インスタンス作成防止 */
	private CommonUtils(){}
	
	/** 登録完了フラグ */
	public final static int COMP_FLG = 1;
	
	/** 登録未完了フラグ */
	public final static int INCOMP_FLG = 0;
	
	/** FLG有効 */
	public final static int EFFECTIVE_FLG = 1;
	
	/** FLG無効 */
	public final static int INVALID_FLG = 0;
	
	/** 0 */
	public final static int ZERO = 0;

}
