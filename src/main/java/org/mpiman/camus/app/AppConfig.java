/**
 * 
 */
package org.mpiman.camus.app;

import javax.sql.DataSource;

import org.seasar.doma.jdbc.Config;
import org.seasar.doma.jdbc.dialect.Dialect;
import org.seasar.doma.jdbc.dialect.MysqlDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 
 * @author mina_saito
 */
@Configuration
public class AppConfig {

	@Autowired
	private DataSource dataSource;

	@Bean
	public Config domaConfig() {
		return new Config() {
			@Override
			public Dialect getDialect() {
				return new MysqlDialect();
			}
			@Override
			public DataSource getDataSource() {
				return dataSource;
			}
		};
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder(){
	    return new BCryptPasswordEncoder();
	}

}