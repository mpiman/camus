package org.mpiman.camus.service;

import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Service;

@Service
public class ProductService {
	
	public String parseUtf8(String name) {
		byte[] bytes = null;
		try {
			bytes = name.getBytes("iso-8859-1");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		try {
			name = new String(bytes, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return name;
	}

}
