package org.mpiman.camus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LogoutController {

	/**
	 * 現在のセッションからログアウトします。
	 * ログアウト後はログイン画面へ遷移します。
	 * 
	 * @param req
	 *            リクエストオブジェクト
	 * @param model
	 *            モデル
	 * @return 表示するhtml名称。
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String index(HttpServletRequest req, Model model) {
		HttpSession session = req.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		return "redirect:/login/user-index";
	}
}
