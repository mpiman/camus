package org.mpiman.camus.controller;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.mpiman.camus.app.common.CommonUtils;
import org.mpiman.camus.persistence.dao.ProductFavoriteDao;
import org.mpiman.camus.persistence.dao.ProductInfoDao;
import org.mpiman.camus.persistence.dao.ProductTypeDao;
import org.mpiman.camus.persistence.dao.TagInfoDao;
import org.mpiman.camus.service.ProductService;
import org.mpiman.camus.persistence.entity.AdminInfo;
import org.mpiman.camus.persistence.entity.ProductFavorite;
import org.mpiman.camus.persistence.entity.ProductInfo;
import org.mpiman.camus.persistence.entity.ProductType;
import org.mpiman.camus.persistence.entity.TagInfo;
import org.mpiman.camus.persistence.entity.UserInfo;
import org.seasar.doma.jdbc.UniqueConstraintException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ProductController {
	
	@Autowired
	ProductInfoDao productInfoDao;
	
	@Autowired
	ProductTypeDao productTypeDao;
	
	@Autowired
	ProductFavoriteDao productFavoriteDao;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	TagInfoDao tagInfoDao;

	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/regist-product", method = RequestMethod.GET)
	public String registProductIndex(@ModelAttribute ProductRegistForm productRegistForm, Model model) {
		return "/admin/regist-product";
	}
	
	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/product/index", method = RequestMethod.GET)
	public String showProductList(RedirectAttributes attr, @ModelAttribute ProductRegistForm productRegistForm, Model model) {
		
		List<ProductInfo> productInfos = productInfoDao.selectAll();
		model.addAttribute("productInfos", productInfos);
		return "/product/index";
	}
	
	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/product/product-detail", method = RequestMethod.POST)
	public String showProductDetail(RedirectAttributes attr, @RequestParam("productId") String productId,
			@ModelAttribute ProductForm productForm, Model model) {
		
		ProductInfo productInfo = productInfoDao.selectById(Integer.parseInt(productId));
		model.addAttribute("productInfo", productInfo);
		
		List<ProductType> productTypes =productTypeDao.selectByProductId(productInfo.getProductId());
		model.addAttribute("productTypes", productTypes);
		return "/product/product-detail";
	}

	/**
	 * 管理者ログイン処理
	 * 
	 * @param webRequest
	 * @param loginForm
	 * @param result
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/product/registar", method = RequestMethod.POST, produces="text/plain;charset=utf-8")
	public String registProduct(HttpSession session,
			@Valid @ModelAttribute("productRegistForm") ProductRegistForm productRegistForm, final BindingResult result,
			Model model) throws Exception {
		// 入力値検証
//		if (result.hasErrors()) {
//			return "login/admin-index";
//		}
		// 商品情報登録
		ProductInfo productInfo = new ProductInfo();
		AdminInfo entity = (AdminInfo)session.getAttribute("admin");
		productInfo.setProductName(productService.parseUtf8(productRegistForm.getProductName()));
		productInfo.setProductPrice(productRegistForm.getPrice());
		productInfo.setProductDetail(productService.parseUtf8(productRegistForm.getProductDetail()));
		productInfo.setProductBrand(productService.parseUtf8(productRegistForm.getProductBland()));
		try {
			productInfo.setProductImg(productRegistForm.getProductImage().getBytes());
		} catch (IOException e) {
			throw new Exception(e);
		}
		productInfo.setFavoriteCount(CommonUtils.ZERO);
		productInfo.setCreateAdminId(entity.getAdminId());
		productInfo.setUpdateAdminId(entity.getAdminId());
		productInfo.setDelFlg(CommonUtils.INVALID_FLG);
		productInfoDao.insert(productInfo);
				
		// 商品詳細情報登録
		List<ProductType> productTypes = new ArrayList<>();
		int cnt = 1;
		for (int typeCnt = 0; typeCnt < productRegistForm.getProductType().length; typeCnt++) {
			ProductType productType = new ProductType();
			productType.setProductTypeId(cnt);
			productType.setProductId(productInfo.getProductId());
			productType.setProductTypeName(productService.parseUtf8(productRegistForm.getProductType()[typeCnt]));
			productType.setProductStockCount(productRegistForm.getStockCount()[typeCnt]);
			productType.setDelFlg(CommonUtils.INVALID_FLG);
			productTypes.add(productType);
			cnt++;
		}
		for (ProductType productType : productTypes) {
			productTypeDao.insert(productType);
		}
		
		// タグ情報登録
		List<TagInfo> tagInfos = new ArrayList<>();
		for (String tag : productRegistForm.getTag()) {
			TagInfo tagInfo = new TagInfo();
			tagInfo.setProductId(productInfo.getProductId());
			tagInfo.setTag(tag);
			tagInfos.add(tagInfo);
		}
		for (TagInfo tagInfo : tagInfos) {
			tagInfoDao.insert(tagInfo);
		}

		return "redirect:/login/admin-index";
	}
	
	@RequestMapping(value = "/product/cartIn", method = RequestMethod.POST)
	public String cartIn(HttpSession session, RedirectAttributes attr, 
			@Valid @ModelAttribute("productForm") ProductForm productForm, final BindingResult result,
			Model model) throws Exception {
		if (productForm.getFlg() == 0) {
			// TODO セッションで保持
			
		} else {
			// お気に入り登録
			UserInfo userInfo = (UserInfo)session.getAttribute("user");
			ProductFavorite productFavorite = new ProductFavorite();
			productFavorite.setProductId(productForm.getProductId());
			productFavorite.setUserId(userInfo.getUserId());
			productFavorite.setRegisterDatetime(LocalDateTime.now());
			try {
				productFavoriteDao.insert(productFavorite);
			} catch(UniqueConstraintException e) {
				attr.addAttribute("errorMsg", "すでにお気に入り登録済みです。");
				return "/product/index";
			}
			
			// 商品のお気に入り回数の更新
			ProductInfo productInfo = productInfoDao.selectById(productForm.getProductId());
			productInfo.setFavoriteCount(productInfo.getFavoriteCount() + 1); 
			productInfoDao.updateFavoriteCount(productInfo);
		}
	
		return "redirect:/product/index";
	}
	
	static class ProductRegistForm implements Serializable {

		/** */
		private static final long serialVersionUID = 1L;

		@NotEmpty(message = "商品名は必須項目です。")
		private String productName;

//		@NotEmpty(message = "小計は必須項目です。")
		private int price;

		@NotEmpty(message = "商品詳細情報は必須項目です。")
		private String productDetail;
		
		private String productBland;
		
		private String[] productType;
		
		private int[] stockCount;
		
		private MultipartFile productImage;
		
		private String[] tag;

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}		

		public String getProductDetail() {
			return productDetail;
		}

		public void setProductDetail(String productDetail) {
			this.productDetail = productDetail;
		}

		public String getProductBland() {
			return productBland;
		}

		public void setProductBland(String productBland) {
			this.productBland = productBland;
		}

		public String[] getProductType() {
			return productType;
		}

		public void setProductType(String[] productType) {
			this.productType = productType;
		}

		public int[] getStockCount() {
			return stockCount;
		}

		public void setStockCount(int[] stockCount) {
			this.stockCount = stockCount;
		}

		public MultipartFile getProductImage() {
			return productImage;
		}

		public void setProductImage(MultipartFile productImage) {
			this.productImage = productImage;
		}

		public String[] getTag() {
			return tag;
		}

		public void setTag(String[] tag) {
			this.tag = tag;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

	}
	
	static class ProductForm {
		
		private int productId;
		
		private int productType;
		
		private int count;
		
		private int flg;

		public int getProductId() {
			return productId;
		}

		public void setProductId(int productId) {
			this.productId = productId;
		}

		public int getProductType() {
			return productType;
		}

		public void setProductType(int productType) {
			this.productType = productType;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		public int getFlg() {
			return flg;
		}

		public void setFlg(int flg) {
			this.flg = flg;
		}
		
	}

}
