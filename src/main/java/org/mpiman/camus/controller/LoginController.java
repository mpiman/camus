
package org.mpiman.camus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.mpiman.camus.persistence.dao.AdminInfoDao;
import org.mpiman.camus.persistence.dao.UserInfoDao;
import org.mpiman.camus.persistence.entity.AdminInfo;
import org.mpiman.camus.persistence.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 
 * @author mina_saito
 */
@Controller
public class LoginController {

	/**
	 * 
	 */
	@Autowired
	PasswordEncoder passwordEncoder;

	/**
	 * 
	 */
	@Autowired
	UserInfoDao userInfoDao;
	
	/**
	 * 
	 */
	@Autowired
	AdminInfoDao adminInfoDao;

	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login/user-index", method = RequestMethod.GET)
	public String userIndex(@ModelAttribute LoginForm loginForm, Model model) {
		return "login/user-index";
	}

	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login/admin-index", method = RequestMethod.GET)
	public String adminIndex(@ModelAttribute LoginForm loginForm, Model model) {
		return "/login/admin-index";
	}
	
	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/index", method = RequestMethod.GET)
	public String adminHomeIndex(@ModelAttribute LoginForm loginForm, Model model) {
		return "/admin/index";
	}

	/**
	 * ユーザログイン処理
	 * 
	 * @param webRequest
	 * @param loginForm
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userLogin/authenticate", method = RequestMethod.POST)
	public String userAuthenticate(HttpServletRequest req, @Valid @ModelAttribute("loginForm") LoginForm loginForm,
			final BindingResult result, Model model) {

		// 入力値検証
		if (result.hasErrors()) {
			return "login/user-index";
		}

		// 認証処理
		UserInfo userInfo = userInfoDao.selectById(loginForm.getId());
		if (userInfo != null && loginForm.getPassword().equals(userInfo.getPassword())) {
			HttpSession session = req.getSession(true);
			session.setAttribute("user", userInfo);
			return "redirect:/"; // my pageへ
		}
		model.addAttribute("errorMsg", "IDまたはパスワードが正しくありません。 もう一度ご確認のうえ、再度入力してください。");
		
		return "login/user-index";
	}
	
	/**
	 * 管理者ログイン処理
	 * 
	 * @param webRequest
	 * @param loginForm
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/adminLogin/authenticate", method = RequestMethod.POST)
	public String adminAuthenticate(RedirectAttributes attr, HttpServletRequest req, @Valid @ModelAttribute("loginForm") LoginForm loginForm,
			final BindingResult result, Model model) {

		// 入力値検証
		if (result.hasErrors()) {
			return "login/admin-index";
		}

		// 認証処理
		AdminInfo adminInfo = adminInfoDao.selectById(loginForm.getId());
		if (adminInfo != null && loginForm.getPassword().equals(adminInfo.getPassword())) {
			HttpSession session = req.getSession(true);
			session.setAttribute("admin", adminInfo);
			return "redirect:/admin/index"; // 管理者topへ
		}
		attr.addFlashAttribute("errorMsg", "IDまたはパスワードが正しくありません。 もう一度ご確認のうえ、再度入力してください。");
		
		return "login/admin-index";
	}

	static class LoginForm {

		@NotEmpty(message = "ログインIDは必須項目です。")
		private String id;

		@NotEmpty(message = "パスワードは必須項目です。")
		private String password;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

}