package org.mpiman.camus.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.mpiman.camus.app.common.CommonUtils;
import org.mpiman.camus.form.UserForm;
import org.mpiman.camus.persistence.dao.ProductFavoriteDao;
import org.mpiman.camus.persistence.dao.ProductInfoDao;
import org.mpiman.camus.persistence.dao.UserInfoDao;
import org.mpiman.camus.persistence.entity.ProductFavorite;
import org.mpiman.camus.persistence.entity.ProductInfo;
import org.mpiman.camus.persistence.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 
 * @author mina_saito
 */
@Controller
public class UserController {

	/**  */
	@Autowired
	UserInfoDao userInfoDao;
	
	@Autowired
	ProductInfoDao productInfoDao;
		
	@Autowired
	ProductFavoriteDao productFavoriteDao;
	
	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	// TODO なおして
	@RequestMapping(value = "/user/regist-user", method = RequestMethod.GET)
	public String registUser(WebRequest webRequest, @ModelAttribute UserRegistForm userRegistForm, Model model) {
		Object userInfo = webRequest.getAttribute("user", WebRequest.SCOPE_SESSION);
		if (userInfo != null) {
			model.addAttribute("userInfo", userInfo);
			model.addAttribute("updateFlg", 1);
		}
		return "user/regist-user";
	}
	
	@RequestMapping(value = "/user/favorite", method = RequestMethod.GET)
	public String favorite(RedirectAttributes attr, HttpSession session, Model model) {
		UserInfo userInfo = (UserInfo)session.getAttribute("user");
		List<ProductInfo> productInfos = new ArrayList<>();
		for (ProductFavorite favorite : productFavoriteDao.selectByUserId(userInfo.getUserId())) {
			productInfos.add(productInfoDao.selectById(favorite.getProductId()));
		}
		model.addAttribute("productInfos", productInfos);
		return "user/favorite";
	}

	/**
	 * 
	 * @param loginForm
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/user/regist-user-detail", method = RequestMethod.GET)
	public String registUserDetail(HttpSession session, @ModelAttribute UserForm userForm, Model model) {
		UserInfo userInfo = (UserInfo) session.getAttribute("user");
		int regCompFlg = userInfo.getRegCompFlg();
		if (regCompFlg == 1) {
			userForm.setFamName(userInfo.getFamNameKanji());
			userForm.setFirName(userInfo.getFirNameKanji());
			userForm.setFamNameKana(userInfo.getFamNameKana());
			userForm.setFirNameKana(userInfo.getFirNameKana());
			userForm.setGender(userInfo.getGender());
			userForm.setYear(userInfo.getBirthdayDate().getYear());
			userForm.setMonth(userInfo.getBirthdayDate().getMonth().getValue());
			userForm.setDay(userInfo.getBirthdayDate().getDayOfMonth());
			userForm.setFirPostalCd(userInfo.getPostalCd().substring(0,3));
			userForm.setLastPostalCd(userInfo.getPostalCd().substring(3,7));
			userForm.setAddress(userInfo.getAddress());
			userForm.setPhone(userInfo.getPhone());
		}
		userForm.setRegCompFlg(regCompFlg);
		model.addAttribute("userForm", userForm);
		return "user/regist-user-detail";
	}

	/**
	 * 管理者ログイン処理
	 * 
	 * @param webRequest
	 * @param loginForm
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/user/registar", method = RequestMethod.POST)
	public String registUser(RedirectAttributes attr, HttpServletRequest req, 
			@Valid @ModelAttribute("userRegistForm") UserRegistForm userRegistForm, final BindingResult result,
			Model model) {
		// 入力値検証
		if (result.hasErrors()) {
			return "user/regist-user";
		}
		UserInfo userInfo = userInfoDao.selectById(userRegistForm.getId());
		if (userInfo != null) {
			attr.addFlashAttribute("errorMsg", "そのIDはすでに使用されています。");
			return "redirect:/user/regist-user";
		}
		if (!userRegistForm.getPassword().equals(userRegistForm.getPasswordConf())) {
			attr.addFlashAttribute("errorMsg", "パスワードと確認用パスワードが一致しません。");
			return "redirect:/user/regist-user";
		}
		UserInfo entity = new UserInfo();
		entity.setUserId(userRegistForm.getId());
		entity.setPassword(userRegistForm.getPassword());
		entity.setRegCompFlg(CommonUtils.INCOMP_FLG);
		userInfoDao.insertPreUserInfo(entity);
		
		HttpSession session = req.getSession(true);
		session.setAttribute("user", userInfoDao.selectById(userRegistForm.getId()));

		attr.addFlashAttribute("infoMsg", "登録が完了しました。");
		return "redirect:/"; // my pageへ
	}

	/**
	 * 管理者ログイン処理
	 * 
	 * @param webRequest
	 * @param loginForm
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/user-detail/registar", method = RequestMethod.POST)
	public String registUserDetail(RedirectAttributes attr, HttpSession session, @Valid @ModelAttribute("userForm") UserForm userForm,
			final BindingResult result, Model model) {
		
		UserInfo entity = (UserInfo)session.getAttribute("user");
		int regCompFlg = entity.getRegCompFlg();
		entity.setFamNameKanji(userForm.getFamName());
		entity.setFirNameKanji(userForm.getFirName());
		entity.setFamNameKana(userForm.getFamNameKana());
		entity.setFirNameKana(userForm.getFirNameKana());
		entity.setGender(userForm.getGender());
		LocalDate birthDate = LocalDate.of(userForm.getYear(), userForm.getMonth(), userForm.getDay());
		entity.setBirthdayDate(birthDate);
		entity.setPostalCd(userForm.getFirPostalCd() + userForm.getLastPostalCd());
		entity.setAddress(userForm.getAddress());
		entity.setPhone(userForm.getPhone());
		entity.setUpdateDatetime(LocalDateTime.now());
		entity.setRegCompFlg(CommonUtils.COMP_FLG);
		
		userInfoDao.update(entity);

		if (regCompFlg == 0) {
			attr.addFlashAttribute("infoMsg", "お届け先情報の登録が完了しました。");
		} else {
			attr.addFlashAttribute("infoMsg", "お届け先情報の更新が完了しました。");
		}

		return "redirect:/";
	}

	static class UserRegistForm {

		@NotEmpty(message = "IDは必須項目です。")
		private String id;

		@NotEmpty(message = "パスワードは必須項目です。")
		private String password;

		@NotEmpty(message = "パスワード（確認用）は必須項目です。")
		private String passwordConf;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getPasswordConf() {
			return passwordConf;
		}

		public void setPasswordConf(String passwordConf) {
			this.passwordConf = passwordConf;
		}

	}

}
