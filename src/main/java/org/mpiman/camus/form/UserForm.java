package org.mpiman.camus.form;

public class UserForm {

	private String famName;

	private String firName;

	private String famNameKana;

	private String firNameKana;

	private int gender;

	private int year;

	private int month;

	private int day;

	private String firPostalCd;

	private String lastPostalCd;

	private String address;

	private String phone;
	
	private int regCompFlg;

	public String getFamName() {
		return famName;
	}

	public void setFamName(String famName) {
		this.famName = famName;
	}

	public String getFirName() {
		return firName;
	}

	public void setFirName(String firName) {
		this.firName = firName;
	}

	public String getFamNameKana() {
		return famNameKana;
	}

	public void setFamNameKana(String famNameKana) {
		this.famNameKana = famNameKana;
	}

	public String getFirNameKana() {
		return firNameKana;
	}

	public void setFirNameKana(String firNameKana) {
		this.firNameKana = firNameKana;
	}
	
	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getFirPostalCd() {
		return firPostalCd;
	}

	public void setFirPostalCd(String firPostalCd) {
		this.firPostalCd = firPostalCd;
	}

	public String getLastPostalCd() {
		return lastPostalCd;
	}

	public void setLastPostalCd(String lastPostalCd) {
		this.lastPostalCd = lastPostalCd;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getRegCompFlg() {
		return regCompFlg;
	}

	public void setRegCompFlg(int regCompFlg) {
		this.regCompFlg = regCompFlg;
	}

}
