package org.mpiman.camus.persistence.entity;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = TagInfoListener.class)
@Table(name = "TAG_INFO")
public class TagInfo {

    /**  */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TAG_ID")
    Integer tagId;

    /**  */
    @Column(name = "PRODUCT_ID")
    Integer productId;

    /**  */
    @Column(name = "TAG")
    String tag;

    /** 
     * Returns the tagId.
     * 
     * @return the tagId
     */
    public Integer getTagId() {
        return tagId;
    }

    /** 
     * Sets the tagId.
     * 
     * @param tagId the tagId
     */
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    /** 
     * Returns the productId.
     * 
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /** 
     * Sets the productId.
     * 
     * @param productId the productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** 
     * Returns the tag.
     * 
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /** 
     * Sets the tag.
     * 
     * @param tag the tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }
}