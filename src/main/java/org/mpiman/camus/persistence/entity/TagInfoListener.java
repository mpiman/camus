package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TagInfoListener implements EntityListener<TagInfo> {

    @Override
    public void preInsert(TagInfo entity, PreInsertContext<TagInfo> context) {
    }

    @Override
    public void preUpdate(TagInfo entity, PreUpdateContext<TagInfo> context) {
    }

    @Override
    public void preDelete(TagInfo entity, PreDeleteContext<TagInfo> context) {
    }

    @Override
    public void postInsert(TagInfo entity, PostInsertContext<TagInfo> context) {
    }

    @Override
    public void postUpdate(TagInfo entity, PostUpdateContext<TagInfo> context) {
    }

    @Override
    public void postDelete(TagInfo entity, PostDeleteContext<TagInfo> context) {
    }
}