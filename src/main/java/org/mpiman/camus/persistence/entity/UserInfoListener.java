package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class UserInfoListener implements EntityListener<UserInfo> {

    @Override
    public void preInsert(UserInfo entity, PreInsertContext<UserInfo> context) {
    }

    @Override
    public void preUpdate(UserInfo entity, PreUpdateContext<UserInfo> context) {
    }

    @Override
    public void preDelete(UserInfo entity, PreDeleteContext<UserInfo> context) {
    }

    @Override
    public void postInsert(UserInfo entity, PostInsertContext<UserInfo> context) {
    }

    @Override
    public void postUpdate(UserInfo entity, PostUpdateContext<UserInfo> context) {
    }

    @Override
    public void postDelete(UserInfo entity, PostDeleteContext<UserInfo> context) {
    }
}