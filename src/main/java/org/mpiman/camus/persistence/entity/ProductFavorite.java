package org.mpiman.camus.persistence.entity;

import java.time.LocalDateTime;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = ProductFavoriteListener.class)
@Table(name = "PRODUCT_FAVORITE")
public class ProductFavorite {

    /**  */
    @Column(name = "USER_ID")
    String userId;

    /**  */
    @Column(name = "PRODUCT_ID")
    Integer productId;

    /**  */
    @Column(name = "REGISTER_DATETIME")
    LocalDateTime registerDatetime;

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the productId.
     * 
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /** 
     * Sets the productId.
     * 
     * @param productId the productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** 
     * Returns the registerDatetime.
     * 
     * @return the registerDatetime
     */
    public LocalDateTime getRegisterDatetime() {
        return registerDatetime;
    }

    /** 
     * Sets the registerDatetime.
     * 
     * @param registerDatetime the registerDatetime
     */
    public void setRegisterDatetime(LocalDateTime registerDatetime) {
        this.registerDatetime = registerDatetime;
    }
}