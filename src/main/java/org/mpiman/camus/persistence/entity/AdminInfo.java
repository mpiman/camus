package org.mpiman.camus.persistence.entity;

import java.time.LocalDateTime;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = AdminInfoListener.class)
@Table(name = "ADMIN_INFO")
public class AdminInfo {

    /**  */
    @Id
    @Column(name = "ADMIN_ID")
    String adminId;

    /**  */
    @Column(name = "PASSWORD")
    String password;

    /**  */
    @Column(name = "CREATE_DATETIME")
    LocalDateTime createDatetime;

    /**  */
    @Column(name = "DEL_FLG")
    byte[] delFlg;

    /** 
     * Returns the adminId.
     * 
     * @return the adminId
     */
    public String getAdminId() {
        return adminId;
    }

    /** 
     * Sets the adminId.
     * 
     * @param adminId the adminId
     */
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    /** 
     * Returns the password.
     * 
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /** 
     * Sets the password.
     * 
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /** 
     * Returns the createDatetime.
     * 
     * @return the createDatetime
     */
    public LocalDateTime getCreateDatetime() {
        return createDatetime;
    }

    /** 
     * Sets the createDatetime.
     * 
     * @param createDatetime the createDatetime
     */
    public void setCreateDatetime(LocalDateTime createDatetime) {
        this.createDatetime = createDatetime;
    }

    /** 
     * Returns the delFlg.
     * 
     * @return the delFlg
     */
    public byte[] getDelFlg() {
        return delFlg;
    }

    /** 
     * Sets the delFlg.
     * 
     * @param delFlg the delFlg
     */
    public void setDelFlg(byte[] delFlg) {
        this.delFlg = delFlg;
    }
}