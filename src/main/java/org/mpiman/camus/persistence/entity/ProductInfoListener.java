package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ProductInfoListener implements EntityListener<ProductInfo> {

    @Override
    public void preInsert(ProductInfo entity, PreInsertContext<ProductInfo> context) {
    }

    @Override
    public void preUpdate(ProductInfo entity, PreUpdateContext<ProductInfo> context) {
    }

    @Override
    public void preDelete(ProductInfo entity, PreDeleteContext<ProductInfo> context) {
    }

    @Override
    public void postInsert(ProductInfo entity, PostInsertContext<ProductInfo> context) {
    }

    @Override
    public void postUpdate(ProductInfo entity, PostUpdateContext<ProductInfo> context) {
    }

    @Override
    public void postDelete(ProductInfo entity, PostDeleteContext<ProductInfo> context) {
    }
}