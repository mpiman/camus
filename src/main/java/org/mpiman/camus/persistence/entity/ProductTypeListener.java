package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ProductTypeListener implements EntityListener<ProductType> {

    @Override
    public void preInsert(ProductType entity, PreInsertContext<ProductType> context) {
    }

    @Override
    public void preUpdate(ProductType entity, PreUpdateContext<ProductType> context) {
    }

    @Override
    public void preDelete(ProductType entity, PreDeleteContext<ProductType> context) {
    }

    @Override
    public void postInsert(ProductType entity, PostInsertContext<ProductType> context) {
    }

    @Override
    public void postUpdate(ProductType entity, PostUpdateContext<ProductType> context) {
    }

    @Override
    public void postDelete(ProductType entity, PostDeleteContext<ProductType> context) {
    }
}