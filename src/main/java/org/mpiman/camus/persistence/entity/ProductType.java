package org.mpiman.camus.persistence.entity;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = ProductTypeListener.class)
@Table(name = "PRODUCT_TYPE")
public class ProductType {

    /**  */
    @Column(name = "PRODUCT_TYPE_ID")
    Integer productTypeId;

    /**  */
    @Column(name = "PRODUCT_ID")
    Integer productId;

    /**  */
    @Column(name = "PRODUCT_TYPE_NAME")
    String productTypeName;

    /**  */
    @Column(name = "PRODUCT_STOCK_COUNT")
    Integer productStockCount;

    /**  */
    @Column(name = "DEL_FLG")
    Integer delFlg;

    /** 
     * Returns the productTypeId.
     * 
     * @return the productTypeId
     */
    public Integer getProductTypeId() {
        return productTypeId;
    }

    /** 
     * Sets the productTypeId.
     * 
     * @param productTypeId the productTypeId
     */
    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    /** 
     * Returns the productId.
     * 
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /** 
     * Sets the productId.
     * 
     * @param productId the productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** 
     * Returns the productTypeName.
     * 
     * @return the productTypeName
     */
    public String getProductTypeName() {
        return productTypeName;
    }

    /** 
     * Sets the productTypeName.
     * 
     * @param productTypeName the productTypeName
     */
    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    /** 
     * Returns the productStockCount.
     * 
     * @return the productStockCount
     */
    public Integer getProductStockCount() {
        return productStockCount;
    }

    /** 
     * Sets the productStockCount.
     * 
     * @param productStockCount the productStockCount
     */
    public void setProductStockCount(Integer productStockCount) {
        this.productStockCount = productStockCount;
    }

    /** 
     * Returns the delFlg.
     * 
     * @return the delFlg
     */
    public Integer getDelFlg() {
        return delFlg;
    }

    /** 
     * Sets the delFlg.
     * 
     * @param delFlg the delFlg
     */
    public void setDelFlg(Integer delFlg) {
        this.delFlg = delFlg;
    }
}