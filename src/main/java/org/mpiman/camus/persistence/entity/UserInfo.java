package org.mpiman.camus.persistence.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = UserInfoListener.class)
@Table(name = "USER_INFO")
public class UserInfo {

    /**  */
    @Id
    @Column(name = "USER_ID")
    String userId;

    /**  */
    @Column(name = "PASSWORD")
    String password;

    /**  */
    @Column(name = "FAM_NAME_KANJI")
    String famNameKanji;

    /**  */
    @Column(name = "FIR_NAME_KANJI")
    String firNameKanji;

    /**  */
    @Column(name = "FAM_NAME_KANA")
    String famNameKana;

    /**  */
    @Column(name = "FIR_NAME_KANA")
    String firNameKana;

    /**  */
    @Column(name = "POSTAL_CD")
    String postalCd;

    /**  */
    @Column(name = "ADDRESS")
    String address;

    /**  */
    @Column(name = "PHONE")
    String phone;

    /**  */
    @Column(name = "BIRTHDAY_DATE")
    LocalDate birthdayDate;

    /**  */
    @Column(name = "GENDER")
    Integer gender;

    /**  */
    @Column(name = "CREATE_DATETIME")
    LocalDateTime createDatetime;

    /**  */
    @Column(name = "UPDATE_DATETIME")
    LocalDateTime updateDatetime;

    /**  */
    @Column(name = "REG_COMP_FLG")
    Integer regCompFlg;

    /**  */
    @Column(name = "DEL_FLG")
    Integer delFlg;

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the password.
     * 
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /** 
     * Sets the password.
     * 
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /** 
     * Returns the famNameKanji.
     * 
     * @return the famNameKanji
     */
    public String getFamNameKanji() {
        return famNameKanji;
    }

    /** 
     * Sets the famNameKanji.
     * 
     * @param famNameKanji the famNameKanji
     */
    public void setFamNameKanji(String famNameKanji) {
        this.famNameKanji = famNameKanji;
    }

    /** 
     * Returns the firNameKanji.
     * 
     * @return the firNameKanji
     */
    public String getFirNameKanji() {
        return firNameKanji;
    }

    /** 
     * Sets the firNameKanji.
     * 
     * @param firNameKanji the firNameKanji
     */
    public void setFirNameKanji(String firNameKanji) {
        this.firNameKanji = firNameKanji;
    }

    /** 
     * Returns the famNameKana.
     * 
     * @return the famNameKana
     */
    public String getFamNameKana() {
        return famNameKana;
    }

    /** 
     * Sets the famNameKana.
     * 
     * @param famNameKana the famNameKana
     */
    public void setFamNameKana(String famNameKana) {
        this.famNameKana = famNameKana;
    }

    /** 
     * Returns the firNameKana.
     * 
     * @return the firNameKana
     */
    public String getFirNameKana() {
        return firNameKana;
    }

    /** 
     * Sets the firNameKana.
     * 
     * @param firNameKana the firNameKana
     */
    public void setFirNameKana(String firNameKana) {
        this.firNameKana = firNameKana;
    }

    /** 
     * Returns the postalCd.
     * 
     * @return the postalCd
     */
    public String getPostalCd() {
        return postalCd;
    }

    /** 
     * Sets the postalCd.
     * 
     * @param postalCd the postalCd
     */
    public void setPostalCd(String postalCd) {
        this.postalCd = postalCd;
    }

    /** 
     * Returns the address.
     * 
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /** 
     * Sets the address.
     * 
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /** 
     * Returns the phone.
     * 
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /** 
     * Sets the phone.
     * 
     * @param phone the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /** 
     * Returns the birthdayDate.
     * 
     * @return the birthdayDate
     */
    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    /** 
     * Sets the birthdayDate.
     * 
     * @param birthdayDate the birthdayDate
     */
    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    /** 
     * Returns the gender.
     * 
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }

    /** 
     * Sets the gender.
     * 
     * @param gender the gender
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /** 
     * Returns the createDatetime.
     * 
     * @return the createDatetime
     */
    public LocalDateTime getCreateDatetime() {
        return createDatetime;
    }

    /** 
     * Sets the createDatetime.
     * 
     * @param createDatetime the createDatetime
     */
    public void setCreateDatetime(LocalDateTime createDatetime) {
        this.createDatetime = createDatetime;
    }

    /** 
     * Returns the updateDatetime.
     * 
     * @return the updateDatetime
     */
    public LocalDateTime getUpdateDatetime() {
        return updateDatetime;
    }

    /** 
     * Sets the updateDatetime.
     * 
     * @param updateDatetime the updateDatetime
     */
    public void setUpdateDatetime(LocalDateTime updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /** 
     * Returns the regCompFlg.
     * 
     * @return the regCompFlg
     */
    public Integer getRegCompFlg() {
        return regCompFlg;
    }

    /** 
     * Sets the regCompFlg.
     * 
     * @param regCompFlg the regCompFlg
     */
    public void setRegCompFlg(Integer regCompFlg) {
        this.regCompFlg = regCompFlg;
    }

    /** 
     * Returns the delFlg.
     * 
     * @return the delFlg
     */
    public Integer getDelFlg() {
        return delFlg;
    }

    /** 
     * Sets the delFlg.
     * 
     * @param delFlg the delFlg
     */
    public void setDelFlg(Integer delFlg) {
        this.delFlg = delFlg;
    }
}