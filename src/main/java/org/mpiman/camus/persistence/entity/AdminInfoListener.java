package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class AdminInfoListener implements EntityListener<AdminInfo> {

    @Override
    public void preInsert(AdminInfo entity, PreInsertContext<AdminInfo> context) {
    }

    @Override
    public void preUpdate(AdminInfo entity, PreUpdateContext<AdminInfo> context) {
    }

    @Override
    public void preDelete(AdminInfo entity, PreDeleteContext<AdminInfo> context) {
    }

    @Override
    public void postInsert(AdminInfo entity, PostInsertContext<AdminInfo> context) {
    }

    @Override
    public void postUpdate(AdminInfo entity, PostUpdateContext<AdminInfo> context) {
    }

    @Override
    public void postDelete(AdminInfo entity, PostDeleteContext<AdminInfo> context) {
    }
}