package org.mpiman.camus.persistence.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ProductFavoriteListener implements EntityListener<ProductFavorite> {

    @Override
    public void preInsert(ProductFavorite entity, PreInsertContext<ProductFavorite> context) {
    }

    @Override
    public void preUpdate(ProductFavorite entity, PreUpdateContext<ProductFavorite> context) {
    }

    @Override
    public void preDelete(ProductFavorite entity, PreDeleteContext<ProductFavorite> context) {
    }

    @Override
    public void postInsert(ProductFavorite entity, PostInsertContext<ProductFavorite> context) {
    }

    @Override
    public void postUpdate(ProductFavorite entity, PostUpdateContext<ProductFavorite> context) {
    }

    @Override
    public void postDelete(ProductFavorite entity, PostDeleteContext<ProductFavorite> context) {
    }
}