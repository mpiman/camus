package org.mpiman.camus.persistence.entity;

import java.time.LocalDateTime;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.GeneratedValue;
import org.seasar.doma.GenerationType;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = ProductInfoListener.class)
@Table(name = "PRODUCT_INFO")
public class ProductInfo {

    /**  */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_ID")
    Integer productId;

    /**  */
    @Column(name = "PRODUCT_NAME")
    String productName;

    /**  */
    @Column(name = "PRODUCT_PRICE")
    Integer productPrice;

    /**  */
    @Column(name = "PRODUCT_DETAIL")
    String productDetail;

    /**  */
    @Column(name = "PRODUCT_BRAND")
    String productBrand;

    /**  */
    @Column(name = "PRODUCT_IMG")
    byte[] productImg;

    /**  */
    @Column(name = "CREATE_DATETIME")
    LocalDateTime createDatetime;

    /**  */
    @Column(name = "CREATE_ADMIN_ID")
    String createAdminId;

    /**  */
    @Column(name = "UPDATE_DATETIME")
    LocalDateTime updateDatetime;

    /**  */
    @Column(name = "UPDATE_ADMIN_ID")
    String updateAdminId;

    /**  */
    @Column(name = "FAVORITE_COUNT")
    Integer favoriteCount;

    /**  */
    @Column(name = "DEL_FLG")
    Integer delFlg;

    /** 
     * Returns the productId.
     * 
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /** 
     * Sets the productId.
     * 
     * @param productId the productId
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** 
     * Returns the productName.
     * 
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /** 
     * Sets the productName.
     * 
     * @param productName the productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /** 
     * Returns the productPrice.
     * 
     * @return the productPrice
     */
    public Integer getProductPrice() {
        return productPrice;
    }

    /** 
     * Sets the productPrice.
     * 
     * @param productPrice the productPrice
     */
    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    /** 
     * Returns the productDetail.
     * 
     * @return the productDetail
     */
    public String getProductDetail() {
        return productDetail;
    }

    /** 
     * Sets the productDetail.
     * 
     * @param productDetail the productDetail
     */
    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    /** 
     * Returns the productBrand.
     * 
     * @return the productBrand
     */
    public String getProductBrand() {
        return productBrand;
    }

    /** 
     * Sets the productBrand.
     * 
     * @param productBrand the productBrand
     */
    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    /** 
     * Returns the productImg.
     * 
     * @return the productImg
     */
    public byte[] getProductImg() {
        return productImg;
    }

    /** 
     * Sets the productImg.
     * 
     * @param productImg the productImg
     */
    public void setProductImg(byte[] productImg) {
        this.productImg = productImg;
    }

    /** 
     * Returns the createDatetime.
     * 
     * @return the createDatetime
     */
    public LocalDateTime getCreateDatetime() {
        return createDatetime;
    }

    /** 
     * Sets the createDatetime.
     * 
     * @param createDatetime the createDatetime
     */
    public void setCreateDatetime(LocalDateTime createDatetime) {
        this.createDatetime = createDatetime;
    }

    /** 
     * Returns the createAdminId.
     * 
     * @return the createAdminId
     */
    public String getCreateAdminId() {
        return createAdminId;
    }

    /** 
     * Sets the createAdminId.
     * 
     * @param createAdminId the createAdminId
     */
    public void setCreateAdminId(String createAdminId) {
        this.createAdminId = createAdminId;
    }

    /** 
     * Returns the updateDatetime.
     * 
     * @return the updateDatetime
     */
    public LocalDateTime getUpdateDatetime() {
        return updateDatetime;
    }

    /** 
     * Sets the updateDatetime.
     * 
     * @param updateDatetime the updateDatetime
     */
    public void setUpdateDatetime(LocalDateTime updateDatetime) {
        this.updateDatetime = updateDatetime;
    }

    /** 
     * Returns the updateAdminId.
     * 
     * @return the updateAdminId
     */
    public String getUpdateAdminId() {
        return updateAdminId;
    }

    /** 
     * Sets the updateAdminId.
     * 
     * @param updateAdminId the updateAdminId
     */
    public void setUpdateAdminId(String updateAdminId) {
        this.updateAdminId = updateAdminId;
    }

    /** 
     * Returns the favoriteCount.
     * 
     * @return the favoriteCount
     */
    public Integer getFavoriteCount() {
        return favoriteCount;
    }

    /** 
     * Sets the favoriteCount.
     * 
     * @param favoriteCount the favoriteCount
     */
    public void setFavoriteCount(Integer favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    /** 
     * Returns the delFlg.
     * 
     * @return the delFlg
     */
    public Integer getDelFlg() {
        return delFlg;
    }

    /** 
     * Sets the delFlg.
     * 
     * @param delFlg the delFlg
     */
    public void setDelFlg(Integer delFlg) {
        this.delFlg = delFlg;
    }
}