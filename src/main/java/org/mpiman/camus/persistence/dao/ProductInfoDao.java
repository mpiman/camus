package org.mpiman.camus.persistence.dao;

import java.util.List;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.ProductInfo;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface ProductInfoDao {

    /**
     * @param productId
     * @return the ProductInfo entity
     */
    @Select
    ProductInfo selectById(Integer productId);
    
    /**
     * @return the ProductInfo entity List
     */
    @Select
    List<ProductInfo> selectAll();
       
    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ProductInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ProductInfo entity);
    
    /**
     * @param entity
     * @return
     */
    @Update(sqlFile=true)
    int updateFavoriteCount(ProductInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ProductInfo entity);
}