package org.mpiman.camus.persistence.dao;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.TagInfo;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface TagInfoDao {

    /**
     * @param tagId
     * @return the TagInfo entity
     */
    @Select
    TagInfo selectById(Integer tagId);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TagInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TagInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TagInfo entity);
}