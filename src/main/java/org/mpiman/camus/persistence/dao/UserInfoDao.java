package org.mpiman.camus.persistence.dao;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.UserInfo;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface UserInfoDao {

    /**
     * @param userId
     * @return the UserInfo entity
     */
    @Select
    UserInfo selectById(String userId);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(UserInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert(sqlFile=true)
    int insertPreUserInfo(UserInfo entity);
    
    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(UserInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(UserInfo entity);
  
}