package org.mpiman.camus.persistence.dao;

import java.util.List;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.ProductFavorite;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface ProductFavoriteDao {

	/**
	 * @param userId
	 * @return
	 */
	@Select
	List<ProductFavorite> selectByUserId(String userId);
	
    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ProductFavorite entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ProductFavorite entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ProductFavorite entity);
}