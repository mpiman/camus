package org.mpiman.camus.persistence.dao;

import java.util.List;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.ProductType;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface ProductTypeDao {

	@Select
	List<ProductType> selectByProductId(int productId);
	
    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ProductType entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ProductType entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ProductType entity);
}