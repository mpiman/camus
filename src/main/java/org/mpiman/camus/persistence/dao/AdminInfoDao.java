package org.mpiman.camus.persistence.dao;

import org.mpiman.camus.persistence.DomaAutowireable;
import org.mpiman.camus.persistence.entity.AdminInfo;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao
@DomaAutowireable
public interface AdminInfoDao {

    /**
     * @param adminId
     * @return the AdminInfo entity
     */
    @Select
    AdminInfo selectById(String adminId);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(AdminInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(AdminInfo entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(AdminInfo entity);
}