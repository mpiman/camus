$(function($) {
  $('#productType').val($(".select-type").val());
  $('#count').val($(".count-type").val());
  var tbl_cnt = 1; // グローバル変数
  
	// 追加ボタンをクリックした時の処理
	$('#add_form').on('click', function() {
		var original = $('#item_' + tbl_cnt);
		tbl_cnt++;
	
		// クローンを作成し、元の後ろに設置。
		$(original)
			.clone()
			.appendTo(original)
			.attr('id', 'item_' + tbl_cnt) // クローンのid属性を変更。
			.find('label').each(function(idx, obj) { // label要素のfor属性を変更。
				$(obj).attr('for', $(obj).attr('for').replace(/_[0-9]+$/, '_' + tbl_cnt));
			})
			.end()
			.find('input').each(function(idx, obj) { // input要素のid,name属性を変更。value値を空白に。
				$(obj).attr({
					id: $(obj).attr('id').replace(/_[0-9]+$/, '_' + tbl_cnt),
					name: $(obj).attr('name').replace(/_[0-9]+$/, '_' + tbl_cnt)
				}).val('');
			});
	});
	
	$(".select-type-dropdown").click(function() {
		$('#productType').val($(".select-type").val());
	});
	
	$(".count-dropdown").click(function() {
		$('#count').val($(".select-count").val());
	});

	$("a").click(function() {
		$(this).val($(".select-count").val());
	});
	
	$('#favorite').click(function() {
		$("#flg").val(1);
	});	
	
	$('#cart-in').click(function() {
		$("#flg").val(0);
	});	
	
});