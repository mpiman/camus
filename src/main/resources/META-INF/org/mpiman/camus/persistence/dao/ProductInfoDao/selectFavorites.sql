select 
	a.PRODUCT_ID,
	b.PRODUCT_NAME,
	b.PRODUCT_PRICE,
	b.PRODUCT_BRAND
from
	PRODUCT_FAVORITE a
join
	PRODUCT_INFO b
on
	a.PRODUCT_ID = b.PRODUCT_ID
order by
	a.REGISTER_DATETIME desc